package me.kazuho.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by zw531 on 2018/2/19. Usage:
 */

@RestController
public class HomeController {
  @RequestMapping("/")
  public String hello() {
    return "Hello, World!";
  }
}
