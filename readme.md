# spring-boot-sample

归档spring-boot学习与使用过程中的案例, 不定期更新.

## 底层依赖

运行所有spring-boot依赖需要安装一些软件, 如构建工具/数据库, 案例中尽可能的不使用这些软件. 下面列出明确需要安装的软件:

- Maven

- MySQL

- Redis

## 案例一览

- spring-boot-aop

  - 使用aop机制拦截HTTP请求

- spring-boot-exception

  - 使用@Advice注解构建全局异常机制

- spring-boot-helloworld

  - 使用最少代码构建spring-boot应用

- spring-boot-jdbc

  - 使用JdbcTemplate进行数据访问

- spring-boot-jpa

  - 使用JPA(Hibernate)进行数据访问

- spring-boot-logback

  - 使用logback进行日志管理

- spring-boot-mail

  - 使用mail插件发送邮件

- spring-boot-mybatis

  - 使用mybatis访问数据库

- spring-boot-redis

  - 使用redisTemplate进行数据访问

- spring-boot-schedule

  - 使用@Schedule进行定时任务配置

- spring-boot-security

  - 使用spring-security进行安全访问

- spring-boot-test

  - 使用spring-test进行测试

- spring-boot-thymeleaf

  - 使用thymeleaf渲染视图


