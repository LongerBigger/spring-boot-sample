package me.kazuho.health;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.InetAddress;

/**
 * Custom health check
 */
@Component
public class CustomHealthCheck implements HealthIndicator {
  private static final String LOCAL_HOST = "127.0.0.1";
  private static final String CAN_NOT_REACH = "255.255.255.255";

  @Override
  public Health health() {
    int responseCode = checkConnection();
    if (responseCode != 0) {
      return Health.down().withDetail("error", "check connection to " + CAN_NOT_REACH + " failed.").build();
    }
    return Health.up().withDetail("message", "check connection to " + LOCAL_HOST + " success.").build();
  }

  private int checkConnection() {
    //  ping timeout
    int timeOut = 3000;
    boolean status;
    try {
      status = InetAddress.getByName(LOCAL_HOST).isReachable(timeOut);
      if (!status) {
        return 1;
      }
    } catch (IOException e) {
      //  TODO: remove it in product code!
      e.printStackTrace();
    }

    return 0;
  }
}
