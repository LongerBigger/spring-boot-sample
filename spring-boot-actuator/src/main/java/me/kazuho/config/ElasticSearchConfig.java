package me.kazuho.config;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import java.net.InetAddress;

/**
 * ElasticSearch config, read config from .yml
 */
@Configuration
public class ElasticSearchConfig {

  @Value("${spring.elasticsearch.host}")
  private String esHost;

  @Value("${spring.elasticsearch.port}")
  private int esPort;

  @Value("${spring.elasticsearch.name}")
  private String esClusterName;

  @Bean
  public Client client() throws Exception {
    Settings settings = Settings.builder().put("cluster.name", esClusterName).build();
    TransportClient client = new PreBuiltTransportClient(settings);
    client.addTransportAddress(new TransportAddress(InetAddress.getByName(esHost), esPort));

    return client;
  }

  @Bean
  public ElasticsearchTemplate elasticsearchTemplate() throws Exception {
    return new ElasticsearchTemplate(client());
  }
}
