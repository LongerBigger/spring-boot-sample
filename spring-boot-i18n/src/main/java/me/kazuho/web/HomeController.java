package me.kazuho.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Locale;

@RestController
@RequestMapping(value = "/")
public class HomeController {

  @Autowired
  private MessageSource messageSource;

  @GetMapping(value = "/welcome")
  public ResponseEntity<?> welcome(@RequestHeader("X-Language") String language) {
    String greeting = null;

    if (language.equalsIgnoreCase("zh_CN")) {
      LocaleContextHolder.setLocale(Locale.CHINA);
    }

    if (language.equalsIgnoreCase("en_US")) {
      LocaleContextHolder.setLocale(Locale.US);
    }

    Locale locale = LocaleContextHolder.getLocale();
    greeting = messageSource.getMessage("home.welcome", null, locale);

    return new ResponseEntity<>(greeting, HttpStatus.OK);
  }
}
