package me.kazuho.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by zw531 on 2018/2/10. Usage:
 */
@Controller
public class ErrorController {

  @RequestMapping("/error/404")
  public String notFoundError() {
    return "exception/404";
  }
}
