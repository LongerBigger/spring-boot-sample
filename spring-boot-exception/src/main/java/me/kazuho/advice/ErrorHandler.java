package me.kazuho.advice;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by zw531 on 2018/2/7. Usage:
 */
@ControllerAdvice
public class ErrorHandler {

  @ExceptionHandler(value = UnknownError.class)
  public void unknownHandler(HttpServletResponse response) throws IOException {
    response.sendRedirect("/error");
  }
}
