package me.kazuho.exception;

/**
 * Created by zw531 on 2018/2/7. Usage:
 */
public class UnknownError extends RuntimeException {
  public UnknownError(String message, Throwable cause) {
    super(message, cause);
  }
}
