package me.kazuho.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import me.kazuho.exception.UnknownError;

/**
 * Created by zw531 on 2018/2/7. Usage:
 */
@Service
public class HomeServiceImpl implements HomeService {

  private static final Logger LOGGER = LoggerFactory.getLogger(HomeServiceImpl.class);

  @Override
  public void hi() {
    try {
      //  cause zero div exception
      int res = 1 / 0;
    } catch (Exception e) {
      LOGGER.error("error!");
      throw new UnknownError("error!", e);
    }
  }
}
