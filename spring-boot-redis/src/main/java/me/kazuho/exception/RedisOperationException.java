package me.kazuho.exception;

public class RedisOperationException extends RuntimeException {
  public RedisOperationException(Throwable cause) {
    super(cause);
  }
}
