package me.kazuho.web;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by zw531 on 2018/1/22.
 * Usage:
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MockServletContext.class)
@WebAppConfiguration
public class UserControllerTest {
    private MockMvc mvc;

    //  build HomeController object
    @Before
    public void setUp() throws Exception{
        mvc = MockMvcBuilders.standaloneSetup(new UserController()).build();
    }


    @Test
    public void testUserController() throws Exception {
        RequestBuilder request = null;

        //  test method list()
        request = get("/user/list");
        mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("[]")));

        //  test method add()
        request = post("/user/add")
                .param("id", "1")
                .param("name", "William")
                .param("age", "24");
        mvc.perform(request)
                .andExpect(content().string(equalTo("add user: 1 success")));

        //  test method get()
        request = get("/user/1/");
        mvc.perform(request)
                .andExpect(content().string(equalTo("User{id=1, name='William', age='24'}")));

        //  test method update()
        request = put("/user/1/update")
                .param("name", "Geralt")
                .param("age", "100");
        mvc.perform(request)
                .andExpect(content().string(equalTo("update user: 1 success")));

        //  test method get()
        request = get("/user/1/");
        mvc.perform(request)
                .andExpect(content().string(equalTo("User{id=1, name='Geralt', age='100'}")));

        //  test method delete()
        request = delete("/user/1/delete");
        mvc.perform(request)
                .andExpect(content().string(equalTo("remove user: 1 success")));

        //  test method list()
        request = get("/user/list");
        mvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("[]")));


    }
}
