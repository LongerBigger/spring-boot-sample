package me.kazuho.model;

/**
 * Created by zw531 on 2017/12/25. Usage:
 */
public class Flash {
  private String title;
  private String content;
  private String exInfo;

  public Flash(String title, String content, String exInfo) {
    this.title = title;
    this.content = content;
    this.exInfo = exInfo;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getExInfo() {
    return exInfo;
  }

  public void setExInfo(String exInfo) {
    this.exInfo = exInfo;
  }
}
