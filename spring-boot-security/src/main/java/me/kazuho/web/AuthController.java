package me.kazuho.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by zw531 on 2017/12/24. Usage:
 */
@Controller
@RequestMapping("/auth")
public class AuthController {

  @RequestMapping("/login")
  public String login() {
    return "auth/login";
  }

}
