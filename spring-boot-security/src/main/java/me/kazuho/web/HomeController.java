package me.kazuho.web;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import me.kazuho.model.Flash;

/**
 * Created by zw531 on 2017/12/5. Usage: home page navigation
 */
@Controller
public class HomeController {

  @RequestMapping("/")
  public String home(Model model) {
    Flash flash = new Flash("测试标题", "测试内容", "只对管理员显示");
    model.addAttribute("flash", flash);
    return "home/index";
  }

}
