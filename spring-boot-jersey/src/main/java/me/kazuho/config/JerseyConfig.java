package me.kazuho.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import me.kazuho.endpoint.AwesomeEndPoint;

@Component
public class JerseyConfig extends ResourceConfig {
  public JerseyConfig() {
    register(AwesomeEndPoint.class);
  }
}
