package me.kazuho.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import javax.transaction.Transactional;

import me.kazuho.dao.UserDao;
import me.kazuho.model.User;

@Service
@Transactional
public class UserServiceImpl implements UserService {
  @Autowired
  private UserDao userDao;

  @Override
  public User create(User user) {
    return userDao.save(user);
  }

  @Override
  public User findById(Integer id) {
    return userDao.findOne(id);
  }

  @Override
  public User update(User user) {
    return userDao.save(user);
  }

  @Override
  public User delete(Integer id) {
    User user = findById(id);
    if (user != null) {
      userDao.delete(user);
    }
    return user;
  }

  @Override
  public List<User> findAll() {
    return userDao.findAll();
  }
}
