package me.kazuho.dao;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import me.kazuho.model.Department;
import me.kazuho.model.Employee;

/**
 * Created by zw531 on 2018/4/9. Usage:
 */
@Component
public class Repository {
  private static final List<Department> departments = new ArrayList<Department>();
  private static final List<Employee> employees = new ArrayList<Employee>();

  public static List<Department> getDepartments() {
    return departments;
  }

  public static List<Employee> getEmployees() {
    return employees;
  }

  public static Department getDepartmentById(Integer deptId) {
    for (Department dept : departments) {
      if (dept.getId().equals(deptId)) {
        return dept;
      }
    }
    return null;
  }
}
