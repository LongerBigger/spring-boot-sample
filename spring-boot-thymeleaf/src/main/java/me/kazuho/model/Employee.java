package me.kazuho.model;

import org.hibernate.validator.constraints.NotEmpty;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import me.kazuho.validation.Option;

/**
 * Created by zw531 on 2018/4/9. Usage:
 */
@Entity
public class Employee {
  @GeneratedValue
  @Id
  private Integer id;
  @NotEmpty(message = Option.NOT_EMPTY)
  private String firstName;
  @NotEmpty(message = Option.NOT_EMPTY)
  private String lastName;
  @NotNull(message = Option.NOT_NULL)
  private String SN;
  @ManyToOne
  private Department department;
  @NotEmpty(message = Option.NOT_EMPTY)
  private Set<Role> roles = new HashSet<Role>();

  @Override
  public String toString() {
    return "Employee{" +
            "id=" + id +
            ", firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", SN='" + SN + '\'' +
            ", department=" + department.toString() +
            '}';
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getSN() {
    return SN;
  }

  public void setSN(String SN) {
    this.SN = SN;
  }

  public Department getDepartment() {
    return department;
  }

  public void setDepartment(Department department) {
    this.department = department;
  }

  public Set<Role> getRoles() {
    return roles;
  }

  public void setRoles(Set<Role> roles) {
    this.roles = roles;
  }
}
