# MySQL5 init sqls

DROP database if exists mybatis;

CREATE database mybatis;

USE mybatis;

DROP TABLE if exists msg;

CREATE TABLE msg (id int primary key auto_increment, title VARCHAR(255), content VARCHAR(255));

INSERT INTO msg(title, content) values ('Msg 1', 'init Msg 1');
