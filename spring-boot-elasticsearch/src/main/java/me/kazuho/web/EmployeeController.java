package me.kazuho.web;

import com.google.gson.Gson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import me.kazuho.dao.EmployeeRepository;
import me.kazuho.document.Employee;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

  private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);
  private static final String chars = "abcdefghijklmnopqrstuvwxyz";

  @Autowired
  private EmployeeRepository repository;

  @Autowired
  private ElasticsearchTemplate template;

  @RequestMapping(value = "/add")
  public String post() {
    Employee employee = new Employee();

    employee.setFirstName(String.valueOf(chars.charAt((int) (Math.random() * 26))));
    employee.setLastName(String.valueOf(chars.charAt((int) (Math.random() * 26))));
    employee.setAge((int) (Math.random() * 120));
    employee.setAbout(String.valueOf(chars.charAt((int) (Math.random() * 26))));
    repository.save(employee);

    LOGGER.info("Persistent employee: " + employee.toString() + " to ElasticSearch.");

    return "success";

  }

  @RequestMapping("/query/{id}")
  public Employee query(@PathVariable("id") String id) {

    Employee accountInfo = repository.queryEmployeeById(id);
    System.err.println(new Gson().toJson(accountInfo));

    return accountInfo;
  }

  @RequestMapping("/query/older")
  public List<Employee> queryOlderThan() {
    return repository.findByAgeGreaterThanEqual(65);
  }

  @RequestMapping("/query/paging")
  public List<Employee> queryPaging() {
    Page<Employee> employees = repository.findAll(PageRequest.of(0, 5));

    return employees.getContent();
  }

  @RequestMapping("/query/sortable")
  public Iterable<Employee> querySortable() {
    return repository.findAll(Sort.by(Sort.Direction.DESC, "age"));
  }


}
