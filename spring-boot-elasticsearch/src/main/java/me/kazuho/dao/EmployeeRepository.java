package me.kazuho.dao;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import me.kazuho.document.Employee;

@Repository
public interface EmployeeRepository extends ElasticsearchRepository<Employee, String> {
  Employee queryEmployeeById(String id);

  List<Employee> findByAgeGreaterThanEqual(Integer age);
}
