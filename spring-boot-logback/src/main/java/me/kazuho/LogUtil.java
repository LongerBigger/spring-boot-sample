package me.kazuho.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by zw531 on 2018/1/8. Usage:
 */
public class LogUtil {
  private static final Logger LOGGER = LoggerFactory.getLogger(LogUtil.class);

  public static void createLog() {
    LOGGER.debug("---DEBUG LOG---");

    LOGGER.info("---INFO LOG---");

    LOGGER.warn("---WARN LOG---");

    LOGGER.error("---ERROR LOG---");
  }
}
